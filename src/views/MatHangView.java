package views;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableModel;

import controllers.MatHangController;
import models.MatHang;
import models.search.MatHangSearch;
import utils.Utils;

public class MatHangView extends JPanel implements ActionListener, ListSelectionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String NAME_TAB = "Mặt Hàng";
	private final String tbCol[] = { "id", "Tên", "SL bán", "Giá Bán", "SL Nhập", "Giá nhập", "Sửa"};
	private CusTextField ten, giaBanTu, giaBanDen, giaNhapTu, giaNhapDen, slBanTu, slBanDen, slNhapTu, slNhapDen;
	private CusTextField _ten, _giaBan, _giaNhap, _slBan, _slNhap;
	private CusTextField __ten, __giaBan, __giaNhap, __slBan, __slNhap, __id;
	private JTable table;
	private JLabel info;
	private JButton searchBtn, cancelSearchBtn, createBtn, updateBtn, deleteBtn;
	private JDialog dialog;
	private MatHangController controller;
	private List<MatHang> list;

	public MatHangView(JTabbedPane tabbedPane) {
		controller = new MatHangController();

		tabbedPane.add(NAME_TAB, this);
		setLayout(new FlowLayout(FlowLayout.LEFT));

		// search form
		ten = new CusTextField(this, "Tên");
		giaBanTu = new CusTextField(this, "Giá Bán từ");
		giaBanDen = new CusTextField(this, "Giá Bán đến");
		giaNhapTu = new CusTextField(this, "Giá Nhập từ");
		giaNhapDen = new CusTextField(this, "Giá Nhập đến");
		slBanTu = new CusTextField(this, "SL Bán từ");
		slBanDen = new CusTextField(this, "SL Bán đến");
		slNhapTu = new CusTextField(this, "SL Nhập từ");
		slNhapDen = new CusTextField(this, "SL Nhập đến");
		searchBtn = new JButton("Lọc");
		searchBtn.addActionListener(this);
		add(searchBtn);
		//
		cancelSearchBtn = new JButton("Hủy lọc");
		cancelSearchBtn.addActionListener(this);
		add(cancelSearchBtn);
		//
		info = new JLabel("Tổng: 0 sản phẩm");
		info.setPreferredSize(new Dimension(1000, 40));
		add(info);

		// tạo bảng
		table = new JTable();
		DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
		tableModel.setColumnIdentifiers(tbCol);
		JScrollPane scrollPane = new JScrollPane(table);
		table.setCellSelectionEnabled(true);
		ListSelectionModel select = table.getSelectionModel();
		select.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		select.addListSelectionListener(this);

		scrollPane.setPreferredSize(new Dimension(1000, 400));
		add(scrollPane);	
		
		// tạo dialog
		dialog = new JDialog((JFrame)SwingUtilities.getRoot(this));
		dialog.setLayout(new FlowLayout(FlowLayout.LEFT));
		dialog.setSize(500, 500);
		__id = new CusTextField(dialog, "Mã");
		__ten = new CusTextField(dialog, "Tên");
		__giaBan = new CusTextField(dialog, "Giá Bán");
		__giaNhap = new CusTextField(dialog, "Giá nhập");
		__slBan = new CusTextField(dialog, "SL Bán");
		__slNhap = new CusTextField(dialog, "SL nhập");
		updateBtn = new JButton("Cập nhật");
		updateBtn.addActionListener(this);
		dialog.add(updateBtn);
		deleteBtn = new JButton("Xóa");
		deleteBtn.addActionListener(this);
		dialog.add(deleteBtn);
		
		// create form
		JLabel createText = new JLabel("Thêm sản phẩm mới");
		createText.setPreferredSize(new Dimension(1000, 40));
		add(createText);
		_ten = new CusTextField(this, "Tên");
		_giaBan = new CusTextField(this, "Giá Bán");
		_giaNhap = new CusTextField(this, "Giá nhập");
		_slBan = new CusTextField(this, "SL Bán");
		_slNhap = new CusTextField(this, "SL nhập");
		createBtn = new JButton("Xác nhận");
		createBtn.addActionListener(this);
		add(createBtn);		
		init();
	}

	public void init() {
		resetSearchField();
		list = controller.search(new MatHangSearch());
		updateData();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source.equals(searchBtn)) {
			searchAction();
		}
		else if (source.equals(cancelSearchBtn)) {
			init();
		}
		else if (source.equals(updateBtn)) {
			updateAction();
		}
		else if (source.equals(createBtn)) {
			createAction();
		}
		else if (source.equals(deleteBtn)) {
			deleteAction();
		}

	}

	public void searchAction() {
		MatHangSearch search = new MatHangSearch();
		search.ten = ten.getValue();
		if (!Utils.isNullOrEmpty(giaBanTu.getValue())) {
			search.giaBanTu = Double.parseDouble(giaBanTu.getValue());
		}
		if (!Utils.isNullOrEmpty(giaBanDen.getValue())) {
			search.giaBanDen = Double.parseDouble(giaBanDen.getValue());
		}
		if (!Utils.isNullOrEmpty(giaNhapTu.getValue())) {
			search.giaNhapTu = Double.parseDouble(giaNhapTu.getValue());
		}
		if (!Utils.isNullOrEmpty(giaNhapDen.getValue())) {
			search.giaNhapDen = Double.parseDouble(giaNhapDen.getValue());
		}
		if (!Utils.isNullOrEmpty(slBanTu.getValue())) {
			search.soLuongBanTu = Integer.parseInt(slBanTu.getValue());
		}
		if (!Utils.isNullOrEmpty(slBanDen.getValue())) {
			search.soLuongBanDen = Integer.parseInt(slBanDen.getValue());
		}
		if (!Utils.isNullOrEmpty(slNhapTu.getValue())) {
			search.soLuongNhapTu = Integer.parseInt(slNhapTu.getValue());
		}
		if (!Utils.isNullOrEmpty(slNhapDen.getValue())) {
			search.soLuongNhapDen = Integer.parseInt(slNhapDen.getValue());
		}
		list = controller.search(search);
		updateData();
	}

	public void updateAction() {
		MatHang mh = new MatHang();
		mh.id = Integer.parseInt(__id.getValue());
		mh.ten = __ten.getValue();
		mh.giaBan = Double.parseDouble(__giaBan.getValue());
		mh.giaNhap = Double.parseDouble(__giaNhap.getValue());
		mh.soLuongBan = Integer.parseInt(__slBan.getValue());
		mh.soLuongNhap = Integer.parseInt(__slNhap.getValue());
		controller.update(mh);
		dialog.setVisible(false);
		init();
	}	

	private void deleteAction() {
		MatHang mh = new MatHang();
		mh.id = Integer.parseInt(__id.getValue());
		controller.delete(mh);	
		dialog.setVisible(false);
		init();
	}

	public void createAction() {
		MatHang mh = new MatHang();
		mh.ten = _ten.getValue();
		mh.giaBan = Double.parseDouble(_giaBan.getValue());
		mh.giaNhap = Double.parseDouble(_giaNhap.getValue());
		mh.soLuongBan = Integer.parseInt(_slBan.getValue());
		mh.soLuongNhap = Integer.parseInt(_slNhap.getValue());
		controller.insert(mh);
		resetCreateField();
		init();
	}

	public void updateData() {
		DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
		tableModel.setRowCount(0);
		for (int i = 0; i < list.size(); i++) {
			Object[] row = new Object[7];
			row = list.get(i).toArray();
			row[6] = "Sửa";
			tableModel.addRow(row);
		}
		info.setText("Tổng: " + list.size() + " sản phẩm");
		table.setModel(tableModel);
		tableModel.fireTableDataChanged();
	}

	public void resetCreateField() {
		_ten.setValue("");
		_giaBan.setValue("");
		_giaNhap.setValue("");
		_slBan.setValue("");
		_slNhap.setValue("");
	}

	public void resetSearchField() {
		ten.setValue("");
		giaBanTu.setValue("");
		giaBanDen.setValue("");
		giaNhapTu.setValue("");
		giaNhapDen.setValue("");
		slBanTu.setValue("");
		slBanDen.setValue("");
		slNhapTu.setValue("");
		slNhapDen.setValue("");
	}
	
	public void setUpdateField(MatHang mh, boolean isReset) {
		__id.setValue(isReset ? "": mh.id);
		__ten.setValue(isReset ? "": mh.ten);
		__giaBan.setValue(isReset ? "": mh.giaBan);
		__giaNhap.setValue(isReset ? "": mh.giaNhap);
		__slBan.setValue(isReset ? "": mh.soLuongBan);
		__slNhap.setValue(isReset ? "": mh.soLuongNhap);
		
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		Object Data = null;
		int[] row = table.getSelectedRows();
		int[] columns = table.getSelectedColumns();
		for (int i = 0; i < row.length; i++) {
			for (int j = 0; j < columns.length; j++) {
				Data = table.getValueAt(row[i], columns[j]);
				
			}
		}
		if(Data == null)
			return;
		if (Data.equals("Sửa")) {
			table.clearSelection();
			MatHang mh = list.get(row[0]);
			setUpdateField(mh, false);
			dialog.setVisible(true);
		}
	}

}
