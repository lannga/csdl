package views;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

public class MainView {
	@SuppressWarnings("unused")
	public MainView(){
		JFrame frame = new JFrame();
		frame.setSize(1000, 800);
		JTabbedPane tabbedPane = new JTabbedPane();
		frame.add(tabbedPane);
		MatHangView matHangView = new MatHangView(tabbedPane);
		NhanVienView nhanVienView = new NhanVienView(tabbedPane);
		KhachHangView khachHangView = new KhachHangView(tabbedPane);
		NhaCungCapView nhaCungCapView= new NhaCungCapView(tabbedPane);
		HoaDonBanView hoaDonNhapView= new HoaDonBanView(tabbedPane);
		frame.setVisible(true);
	}
}
