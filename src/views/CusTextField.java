package views;

import java.awt.Component;
import java.awt.Container;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CusTextField extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel label;
	private JTextField textField;
	
	public CusTextField(Container container, String labelName) {
		super();
		container.add(this);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		label =new JLabel(labelName);  
		textField = new JTextField("",10);
		this.add(label);
		this.add(textField);
	}
	
	public CusTextField(Container container, String labelName, String defaultValue) {
		this(container, labelName);
		this.textField.setText(defaultValue);
	}
	
	public CusTextField(Container container, String labelName, boolean horizontal) {
		this(container, labelName);
		if(horizontal)
			setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
	}
	
	
	public String getValue() {
		return textField.getText();
	}
	
	public void setValue(Object value) {
		textField.setText(String.valueOf(value));
	}


	
		
}
