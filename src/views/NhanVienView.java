package views;


import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import controllers.NhanVienController;
import models.NhanVien;
import models.search.NhanVienSearch;
import utils.Utils;

public class NhanVienView extends JPanel implements ActionListener, ListSelectionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String NAME_TAB = "Nhân Viên";
	private final String tbCol[] = { "Id", "Tên", "Địa Chỉ", "Status", "Số Điện Thoại", "Sửa"};
	private CusTextField ten, diaChi, status, sdt;
	private CusTextField _ten, _diaChi, _status, _sdt;
	private CusTextField __ten, __diaChi, __status, __sdt , __id;
	private JTable table;
	private JLabel info;
	private JButton searchBtn, cancelSearchBtn, createBtn, updateBtn, deleteBtn;
	private JDialog dialog;
	private NhanVienController controller;
	private List<NhanVien> list;

	public NhanVienView(JTabbedPane tabbedPane) {
		controller = new NhanVienController();

		tabbedPane.add(NAME_TAB, this);
		setLayout(new FlowLayout(FlowLayout.LEFT));

		// search form
		ten = new CusTextField(this, "Tên");
		diaChi = new CusTextField(this, "Địa Chỉ");
		status = new CusTextField(this, "Status");
		sdt = new CusTextField(this, "Số Điện Thoại");
		
		searchBtn = new JButton("Lọc");
		searchBtn.addActionListener(this);
		add(searchBtn);
		//
		cancelSearchBtn = new JButton("Hủy lọc");
		cancelSearchBtn.addActionListener(this);
		add(cancelSearchBtn);
		//
		info = new JLabel("Tổng: 0 sản phẩm");
		info.setPreferredSize(new Dimension(1000, 40));
		add(info);

		// tạo bảng
		table = new JTable();
		DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
		tableModel.setColumnIdentifiers(tbCol);
		JScrollPane scrollPane = new JScrollPane(table);
		table.setCellSelectionEnabled(true);
		ListSelectionModel select = table.getSelectionModel();
		select.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		select.addListSelectionListener(this);
		
		
		scrollPane.setPreferredSize(new Dimension(1000, 400));
		add(scrollPane);	
		
		
		// tạo dialog
		dialog = new JDialog((JFrame)SwingUtilities.getRoot(this));
		dialog.setLayout(new FlowLayout(FlowLayout.LEFT));
		dialog.setSize(500, 500);
		__id = new CusTextField(dialog, "Mã");
		__ten = new CusTextField(dialog, "Tên");
		__status = new CusTextField(dialog, "Status");
		__diaChi = new CusTextField(dialog, "Địa Chỉ");
		__sdt = new CusTextField(dialog, "Số Điện Thoại ");
		updateBtn = new JButton("Cập nhật");
		updateBtn.addActionListener(this);
		dialog.add(updateBtn);
		deleteBtn = new JButton("Xóa");
		deleteBtn.addActionListener(this);
		dialog.add(deleteBtn);
		
		// create form
		JLabel createText = new JLabel("Thêm nhân viên mới");
		createText.setPreferredSize(new Dimension(1000, 40));
		add(createText);
		_ten = new CusTextField(this, "Tên");
		_diaChi = new CusTextField(this, "Địa Chỉ");
		_sdt = new CusTextField(this, "Số Điện Thoại");
		_status = new CusTextField(this, "Status");

		createBtn = new JButton("Xác nhận");
		createBtn.addActionListener(this);
		add(createBtn);		
		init();
	}

	public void init() {
		resetSearchField();
		list = controller.search(new NhanVienSearch());
		updateData();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source.equals(searchBtn)) {
			searchAction();
		}
		else if (source.equals(cancelSearchBtn)) {
			init();
		}
		else if (source.equals(updateBtn)) {
			updateAction();
		}
		else if (source.equals(createBtn)) {
			createAction();
		}
		else if (source.equals(deleteBtn)) {
			deleteAction();
		}

	}

	public void searchAction() {
		NhanVienSearch search = new NhanVienSearch();
		search.tenNv = ten.getValue();
		search.diaChiNv=diaChi.getValue();
		search.statusNv=status.getValue();
		search.sdtNv=sdt.getValue();
		list = controller.search(search);
		updateData();
	}

	public void updateAction() {
		NhanVien nv = new NhanVien();
		nv.idNv = Integer.parseInt(__id.getValue());
		nv.tenNv = __ten.getValue();
		nv.diaChiNv=__diaChi.getValue();
		nv.statusNv=__status.getValue();
		nv.sdtNv=__sdt.getValue();
		
		controller.update(nv);
		dialog.setVisible(false);
		init();
	}	

	private void deleteAction() {
		NhanVien nv = new NhanVien();
		nv.idNv = Integer.parseInt(__id.getValue());
		controller.delete(nv);	
		dialog.setVisible(false);
		init();
	}

	public void createAction() {
		
		NhanVien nv = new NhanVien();
		int t;
		if(list.size()>0) {
			 t=list.get(list.size()-1).idNv;
		}else {
			t=-1;
		}
		nv.idNv=t+1;
		nv.tenNv = _ten.getValue();
		nv.diaChiNv=_diaChi.getValue();
		nv.statusNv=_status.getValue();
		nv.sdtNv=_sdt.getValue();
		controller.insert(nv);
		resetCreateField();
		init();
	}

	public void updateData() {
		DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
		tableModel.setRowCount(0);
		for (int i = 0; i < list.size(); i++) {
			Object[] row = new Object[6];
			row = list.get(i).toArray();
			row[5] = "Sửa";
			tableModel.addRow(row);
		}
		info.setText("Tổng: " + list.size() + " sản phẩm");
		table.setModel(tableModel);
		tableModel.fireTableDataChanged();
	}

	public void resetCreateField() {
		_ten.setValue("");
		_diaChi.setValue("");	
		_status.setValue("");
		_sdt.setValue("");
	}

	public void resetSearchField() {
		ten.setValue("");
		diaChi.setValue("");
		status.setValue("");
		sdt.setValue("");
		
	}
	
	public void setUpdateField(NhanVien nv, boolean isReset) {
		__id.setValue(isReset ? "": nv.idNv);
		__ten.setValue(isReset ? "": nv.tenNv);
		__diaChi.setValue(isReset ? "": nv.diaChiNv);
		__status.setValue(isReset ? "": nv.statusNv);
		__sdt.setValue(isReset ? "": nv.sdtNv);
	
		
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		Object Data = null;
		int[] row = table.getSelectedRows();
		int[] columns = table.getSelectedColumns();
		for (int i = 0; i < row.length; i++) {
			for (int j = 0; j < columns.length; j++) {
				Data = table.getValueAt(row[i], columns[j]);
				
			}
		}
		if(Data == null)
			return;
		if (Data.equals("Sửa")) {
			table.clearSelection();
			NhanVien nv = list.get(row[0]);
			setUpdateField(nv, false);
			dialog.setVisible(true);
		}
	}

}

