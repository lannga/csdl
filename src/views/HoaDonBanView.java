package views;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import controllers.HoaDonBanController;
import controllers.NhaCungCapController;
import models.HoaDonBan;
import models.NhaCungCap;
import models.search.HoaDonBanSearch;
import models.search.NhaCungCapSearch;

public class HoaDonBanView extends JPanel implements ActionListener, ListSelectionListener{
	
	private static final long serialVersionUID = 1L;
	private static final String NAME_TAB = "Hóa đơn bán";
	private final String tbCol[] = { "Id", "Khách hàng", "Số lượng","Thành tiền", "Nhân viên bán","Chi tiết"};
	private CusTextField ten, diaChi, sdt;
	private CusTextField _ten, _diaChi, _sdt;
	private CusTextField __ten, __diaChi,  __sdt , __id;
	
	private JTable table;
	private JLabel info;
	private JButton searchBtn, cancelSearchBtn, createBtn, updateBtn, deleteBtn;
	private JDialog dialog;
	private HoaDonBanController controller;
	
	private List<HoaDonBan> list;
	
	public HoaDonBanView(JTabbedPane tabbedPane) {
		controller = new HoaDonBanController();

		tabbedPane.add(NAME_TAB, this);
		setLayout(new FlowLayout(FlowLayout.LEFT));

		// search form
		ten = new CusTextField(this, "Tên");
		diaChi = new CusTextField(this, "Địa Chỉ");
		sdt = new CusTextField(this, "Số Điện Thoại");
		
		searchBtn = new JButton("Lọc");
		searchBtn.addActionListener(this);
		add(searchBtn);
		//
		cancelSearchBtn = new JButton("Hủy lọc");
		cancelSearchBtn.addActionListener(this);
		add(cancelSearchBtn);
		//
		info = new JLabel("Tổng: 0 sản phẩm");
		info.setPreferredSize(new Dimension(1000, 40));
		add(info);

		// tạo bảng
		table = new JTable();
		DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
		tableModel.setColumnIdentifiers(tbCol);
		JScrollPane scrollPane = new JScrollPane(table);
		table.setCellSelectionEnabled(true);
		ListSelectionModel select = table.getSelectionModel();
		select.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		select.addListSelectionListener(this);
		
		
		scrollPane.setPreferredSize(new Dimension(1000, 400));
		add(scrollPane);	
		
		
		// tạo dialog
		dialog = new JDialog((JFrame)SwingUtilities.getRoot(this));
		dialog.setLayout(new FlowLayout(FlowLayout.LEFT));
		dialog.setSize(500, 500);
		__id = new CusTextField(dialog, "Mã");
		__ten = new CusTextField(dialog, "Tên");
		__diaChi = new CusTextField(dialog, "Địa Chỉ");
		__sdt = new CusTextField(dialog, "Số Điện Thoại ");
		updateBtn = new JButton("Cập nhật");
		updateBtn.addActionListener(this);
		dialog.add(updateBtn);
		deleteBtn = new JButton("Xóa");
		deleteBtn.addActionListener(this);
		dialog.add(deleteBtn);
		
		// create form
		JLabel createText = new JLabel("Thêm nhà cung cấp mới");
		createText.setPreferredSize(new Dimension(1000, 40));
		add(createText);
		_ten = new CusTextField(this, "Tên");
		_diaChi = new CusTextField(this, "Địa Chỉ");
		_sdt = new CusTextField(this, "Số Điện Thoại");
		createBtn = new JButton("Xác nhận");
		createBtn.addActionListener(this);
		add(createBtn);		
		init();
	}
	
	public void init() {
		resetSearchField();
		list = controller.search(new HoaDonBanSearch());
		updateData();
	}
	
	public void updateData() {
		DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
		tableModel.setRowCount(0);
		for (int i = 0; i < list.size(); i++) {
			Object[] row = new Object[6];
			row = list.get(i).toArray();
			row[5] = "Xem";
			tableModel.addRow(row);
		}
		info.setText("Tổng: " + list.size() + " hóa đơn");
		table.setModel(tableModel);
		tableModel.fireTableDataChanged();
	}
	
	public void resetSearchField() {
		ten.setValue("");
		diaChi.setValue("");
		sdt.setValue("");
		
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

}
