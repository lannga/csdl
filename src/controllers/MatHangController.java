package controllers;

import java.util.List;

import models.MatHang;
import models.search.MatHangSearch;

public class MatHangController {
	public List<MatHang> search(MatHangSearch search){
		MatHang mh = new MatHang();		
		return mh.query(search);
	}
	
	public void insert(MatHang mh) {
		mh.insert(mh);
	}
	
	public void delete(MatHang mh) {
		mh.delete(mh, null);
	}
	
	public void update(MatHang mh) {
		mh.update(mh, null, 0);
	}
}
