package controllers;

import java.util.List;

import models.HoaDonBan;
import models.search.HoaDonBanSearch;

public class HoaDonBanController{
	public List<HoaDonBan> search(HoaDonBanSearch search){
		HoaDonBan nv = new HoaDonBan();		
		return nv.query(search);
	}
	
	public void insert(HoaDonBan ncc) {
		ncc.insert(ncc);
	}
	
	public void delete(HoaDonBan ncc) {
		ncc.delete(ncc, null);
	}
	
	public void update(HoaDonBan ncc) {
		ncc.update(ncc, null, 0);
	}

}
