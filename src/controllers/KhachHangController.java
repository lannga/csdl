package controllers;

import java.util.List;

import models.KhachHang;
import models.search.KhachHangSearch;


public class KhachHangController {
	public List<KhachHang> search(KhachHangSearch search){
		KhachHang nv = new KhachHang();		
		return nv.query(search);
	}
	
	public void insert(KhachHang ncc) {
		ncc.insert(ncc);
	}
	
	public void delete(KhachHang ncc) {
		ncc.delete(ncc, null);
	}
	
	public void update(KhachHang ncc) {
		ncc.update(ncc, null, 0);
	}
}

