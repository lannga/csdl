package controllers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import models.BaseModel;
import models.KhachHang;
import models.search.KhachHangSearch;

//k dung den nua - testing
public abstract class GeneralController <X,Y> extends BaseModel<X, Y>{
	
	
	public List<X> search(Y search){
		BaseModel<X, Y> baseModel = new BaseModel<X,Y>() {

			@Override
			public X buildObject(ResultSet rs) throws SQLException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String buildCondition(Y m) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String buildQuery(Y m) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String buildUpdate(X t, Y m) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String buildInsert(X t) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String buildDelete(X t, Y m) {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
		return baseModel.query(search);
	}
	
	public void insert(GeneralController ncc) {
		ncc.insert(ncc);
	}
	
	public void delete(GeneralController ncc) {
		ncc.delete(ncc, null);
	}
	
	public void update(GeneralController ncc) {
		ncc.update(ncc, null, 0);
	}

}
