package controllers;

import java.util.List;

import models.NhanVien;
import models.search.NhanVienSearch;

public class NhanVienController {
	public List<NhanVien> search(NhanVienSearch search){
		NhanVien nv = new NhanVien();		
		return nv.query(search);
	}
	
	public void insert(NhanVien nv) {
		nv.insert(nv);
	}
	
	public void delete(NhanVien nv) {
		nv.delete(nv, null);
	}
	
	public void update(NhanVien nv) {
		nv.update(nv, null, 0);
	}
}
