package controllers;

import java.util.List;

import models.NhaCungCap;
import models.search.NhaCungCapSearch;

public class NhaCungCapController {
	public List<NhaCungCap> search(NhaCungCapSearch search){
		NhaCungCap nv = new NhaCungCap();		
		return nv.query(search);
	}
	
	public void insert(NhaCungCap ncc) {
		ncc.insert(ncc);
	}
	
	public void delete(NhaCungCap ncc) {
		ncc.delete(ncc, null);
	}
	
	public void update(NhaCungCap ncc) {
		ncc.update(ncc, null, 0);
	}
}
