package utils;

public class Utils {
	public static boolean isNullOrEmpty(String s) {
		return s.equals(null) || s.equals("");
	}
}
