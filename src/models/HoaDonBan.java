package models;
import java.sql.ResultSet;
import java.sql.SQLException;

import models.search.HoaDonBanSearch;
import models.search.KhachHangSearch;
import utils.Utils;

public class HoaDonBan extends BaseModel<HoaDonBan, HoaDonBanSearch>{

	private int id;
	private int tongSP;
	private int tongTien;
	private String khachHang;
	private String nhanVienBan;
	
	public HoaDonBan(int i, int tongsp, int tongtien, String tenKH, String tenNV) {
		id = i;
		tongSP = tongsp;
		tongTien = tongtien;
		khachHang = tenKH;
		nhanVienBan = tenNV;
	}
	
	public HoaDonBan() {
		
	}


	public Object[] toArray() {
		Object arr[] = {id,tongSP,tongTien,khachHang ,nhanVienBan,""};
		return arr;
	}
	@Override
	public HoaDonBan buildObject(ResultSet rs) throws SQLException {
		HoaDonBan ncc = new HoaDonBan(
				rs.getInt(1),
				rs.getInt(2),
				rs.getInt(3),
				rs.getString(4),
				rs.getString(5)
				);		
		return ncc;
	}
	@Override
	public String buildCondition(HoaDonBanSearch m) {
		String query = " WHERE 1 = 1";
//		if(m.idKH !=0) {
//			query += " AND ID_KH = " +m.idKH; 
//		}
//		if(!Utils.isNullOrEmpty(m.diaChiKH)) {
//			query += " AND ADDRESS LIKE N'%" +m.diaChiKH+"%'"; 
//		}
//		
//		if(!Utils.isNullOrEmpty(m.tenKH)) {
//			query += " AND NAME LIKE N'%" +m.tenKH+"%'"; 
//		}
//		if(!Utils.isNullOrEmpty(m.sdtKH)) {
//			query += " AND PHONE LIKE N'%" +m.sdtKH+"%'"; 
//		}
		
		return query;
	}
	@Override
	public String buildQuery(HoaDonBanSearch m) {
		String sql = "SELECT * FROM BILLSALE ";
		sql += buildCondition(m);
		return sql;
	}
	@Override
	public String buildUpdate(HoaDonBan t, HoaDonBanSearch s) {
		String sql = "UPDATE BILLSALE SET";
//		if(t.tenKH != null) {
//			sql += " NAME =" + "N'"+ t.tenKH+"'"+",";
//		}	
//		
//		if(t.diaChiKH != null) {
//			sql += " ADDRESS =" + "N'"+ t.diaChiKH+"'"+",";
//		}	
//		
//		
//		if(t.sdtKH != null) {
//			sql += " PHONE =" + "'"+ t.sdtKH+"'";
//		}	
//		
//		if(s == null) {
//			sql += " WHERE ID_KH = "+ t.idKH;
//		}else {
//			sql += buildCondition(s);
//		}
//		System.out.println(sql);
		return sql;
	}
	@Override
	public String buildInsert(HoaDonBan t) {
//		String sql = "INSERT INTO BILLSALE (ID_KH,NAME,ADDRESS,PHONE)" +
//                "VALUES ("
//				+t.idKH
//				+" ,"+" N'"+t.tenKH +"'"
//				+" ,"+" N'"+t.diaChiKH +"'"
//				+" ,"+" '"+t.sdtKH+"'"
//				+")";
//		System.out.println(sql);
//		return sql;
		return null;
	}
	@Override
	public String buildDelete(HoaDonBan t, HoaDonBanSearch search) {
		String sql = "DELETE FROM BILLSALE";
//		if(search == null) {
//			sql += " WHERE ID_KH = "+ t.idKH;
//		}else {
//			sql += buildCondition(search);
//		}
		return sql;	
	}

}
