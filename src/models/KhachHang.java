package models;

import java.sql.ResultSet;
import java.sql.SQLException;

import models.search.KhachHangSearch;
import utils.Utils;

public class KhachHang extends BaseModel<KhachHang, KhachHangSearch> {
	
	public int idKH;
	public String tenKH;
	public String diaChiKH;
	public String sdtKH;
	

	public KhachHang(int idKH, String tenKH,String diaChiKH,String sdtKH) {
		this.idKH = idKH;
		this.tenKH=tenKH;
		this.diaChiKH=diaChiKH;
		
		this.sdtKH=sdtKH;
	}
	
	public KhachHang() {
		
	}
	
	
	
	public Object[] toArray() {
		Object arr[] = {idKH,tenKH,diaChiKH,sdtKH ,""};
		return arr;
	}
	@Override
	public KhachHang buildObject(ResultSet rs) throws SQLException {
		KhachHang ncc = new KhachHang(
				rs.getInt(1),
				rs.getString(2),
				rs.getString(3),
				rs.getString(4)
				);		
		return ncc;
	}
	@Override
	public String buildCondition(KhachHangSearch m) {
		String query = " WHERE 1 = 1";
		if(m.idKH !=0) {
			query += " AND ID_KH = " +m.idKH; 
		}
		if(!Utils.isNullOrEmpty(m.diaChiKH)) {
			query += " AND ADDRESS LIKE N'%" +m.diaChiKH+"%'"; 
		}
		
		if(!Utils.isNullOrEmpty(m.tenKH)) {
			query += " AND NAME LIKE N'%" +m.tenKH+"%'"; 
		}
		if(!Utils.isNullOrEmpty(m.sdtKH)) {
			query += " AND PHONE LIKE N'%" +m.sdtKH+"%'"; 
		}
		
		return query;
	}
	@Override
	public String buildQuery(KhachHangSearch m) {
		String sql = "SELECT * FROM CUSTOMER ";
		sql += buildCondition(m);
		return sql;
	}
	@Override
	public String buildUpdate(KhachHang t, KhachHangSearch s) {
		String sql = "UPDATE CUSTOMER SET";
		if(t.tenKH != null) {
			sql += " NAME =" + "N'"+ t.tenKH+"'"+",";
		}	
		
		if(t.diaChiKH != null) {
			sql += " ADDRESS =" + "N'"+ t.diaChiKH+"'"+",";
		}	
		
		
		if(t.sdtKH != null) {
			sql += " PHONE =" + "'"+ t.sdtKH+"'";
		}	
		
		if(s == null) {
			sql += " WHERE ID_KH = "+ t.idKH;
		}else {
			sql += buildCondition(s);
		}
		System.out.println(sql);
		return sql;
	}
	@Override
	public String buildInsert(KhachHang t) {
		String sql = "INSERT INTO CUSTOMER (ID_KH,NAME,ADDRESS,PHONE)" +
                "VALUES ("
				+t.idKH
				+" ,"+" N'"+t.tenKH +"'"
				+" ,"+" N'"+t.diaChiKH +"'"
				+" ,"+" '"+t.sdtKH+"'"
				+")";
		System.out.println(sql);
		return sql;
	}
	@Override
	public String buildDelete(KhachHang t, KhachHangSearch search) {
		String sql = "DELETE FROM CUSTOMER";
		if(search == null) {
			sql += " WHERE ID_KH = "+ t.idKH;
		}else {
			sql += buildCondition(search);
		}
		return sql;
	}

	

	
		
	

	
}



