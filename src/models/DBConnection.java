package models;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.sql.Statement;

public class DBConnection {
	private static Connection connection = null;	  
    static{         
//        String dbURL = "jdbc:sqlserver://localhost\\csdl"; //sql-server
    	String dbURL = "jdbc:mysql://sql12.freemysqlhosting.net:3306/sql12351410?verifyServerCertificate=false&useSSL=false";  // mysql
        String user = "sql12351410";
        String pass = "7mnnUK4ygU";
        try {
            connection = DriverManager.getConnection(dbURL, user, pass); 
            System.out.println("MYSQL SERVER !!!");
        } 
        catch (Exception e) {
        	System.out.println("SQL SERVER !!!");
        	SQLSyntaxErrorException sqlErr =(SQLSyntaxErrorException) e;
        	if(sqlErr.getErrorCode() == 1049) {
        		String url = "jdbc:mysql://localhost:3306?verifyServerCertificate=false&useSSL=false"; // mysql
//        		String url = "jdbc:sqlserver://localhost"; // sql-server
        		try {
					Connection conn = DriverManager.getConnection(url, user, pass);
					Statement smt = conn.createStatement();
					smt.execute("CREATE DATABASE csdl");
					conn.close();
					connection = DriverManager.getConnection(dbURL, user, pass); 
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
        		
        	}
        }
    } 
    
    public static Connection getConnection(){ 
        return connection; 
    }
}
