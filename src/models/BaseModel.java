package models;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseModel<T, M> {
	private static Connection conn = DBConnection.getConnection();
	private static Statement stm;

	private static final int INSERT = 1;
	private static final int UPDATE = 2;
	private static final int DELETE = 3;

	public abstract T buildObject(ResultSet rs) throws SQLException;

	public abstract String buildCondition(M m);
	
	public abstract String buildQuery(M m);

	public abstract String buildUpdate(T t, M m);

	public abstract String buildInsert(T t);

	public abstract String buildDelete(T t, M m);

	public BaseModel(){
		
	}
	
	public List<T> query(M m) {
		String sql = buildQuery(m);
		List<T> list = new ArrayList<T>();
		try {
			stm = conn.createStatement();
			ResultSet rs = stm.executeQuery(sql);
			while (rs.next()) {
				list.add(buildObject(rs));
			}
			stm.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	
	public T queryOne(M m) {
		List<T> list = query(m);
		return list.get(0);
	}

	public void update(T t, M m, int type) {
		String sql = "";
		switch (type) {
		case INSERT:
			sql = buildInsert(t);
			break;
		case DELETE:
			sql = buildDelete(t, m);
			break;
		default:
			sql = buildUpdate(t, m);
			break;
		}
		try {
			stm = conn.createStatement();
			stm.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void insert(T t) {
		update(t , null, INSERT);
	}
	
	public void delete(T t, M m) {
		update(t, m, DELETE);
	}
	

}
