package models;

import java.sql.ResultSet;
import java.sql.SQLException;

import models.search.MatHangSearch;
import utils.Utils;

public class MatHang extends BaseModel<MatHang, MatHangSearch> {
	
	public int id;
	public String ten;
	public int soLuongBan = -1, soLuongNhap = -1;
	public double giaBan = -1, giaNhap = -1;
	
	

	public MatHang(int id, String ten, int soLuongBan, int soLuongNhap, double giaBan, double giaNhap) {
		this.id = id;
		this.ten = ten;
		this.soLuongBan = soLuongBan;
		this.soLuongNhap = soLuongNhap;
		this.giaBan = giaBan;
		this.giaNhap = giaNhap;
	}
	
	public MatHang() {
		
	}
	
	public Object[] toArray() {
		Object arr[] = {id,ten,soLuongBan,giaBan ,soLuongNhap ,giaNhap,""};
		return arr;
	}
	
		
	@Override
	public MatHang buildObject(ResultSet rs) throws SQLException{
		MatHang mh = new MatHang(
				rs.getInt("id"),
				rs.getString("ten"),
				rs.getInt("soLuongBan"),
				rs.getInt("soLuongNhap"),
				rs.getDouble("giaBan"),
				rs.getDouble("giaNhap")
				);		
		return mh;
	}


	@Override
	public String buildQuery(MatHangSearch m) {
		String sql = "SELECT * FROM mathang ";
		sql += buildCondition(m);
		return sql;
	}

	@Override
	public String buildUpdate(MatHang t, MatHangSearch s) {
		String sql = "UPDATE mathang SET";
		if(t.ten != null) {
			sql += " ten =" + "'"+ t.ten+"'";
		}	
		if(t.giaBan != -1) {
			sql += " , giaBan =" + t.giaBan;
		}
		if(t.giaNhap != -1) {
			sql += " , giaNhap =" + t.giaNhap;
		}
		if(t.soLuongBan != -1) {
			sql += " , soLuongBan =" + t.soLuongBan;
		}
		if(t.soLuongNhap != -1) {
			sql += " , soLuongNhap =" + t.soLuongNhap;
		}
		if(s == null) {
			sql += " WHERE id = "+ t.id;
		}else {
			sql += buildCondition(s);
		}
		return sql;
	}

	@Override
	public String buildInsert(MatHang t) {
		String sql = "INSERT INTO mathang (`ten`, `soLuongBan`, `giaBan`, `soLuongNhap`, `giaNhap`)" +
                "VALUES ("
				+" '"+t.ten+"'"
				+" ,"+t.soLuongBan 
				+" ,"+t.giaBan 
				+" ,"+t.soLuongNhap
				+" ,"+t.giaNhap
				+")";
		return sql;
	}

	@Override
	public String buildDelete(MatHang t, MatHangSearch search) {
		String sql = "DELETE FROM mathang";
		if(search == null) {
			sql += " WHERE id = "+ t.id;
		}else {
			sql += buildCondition(search);
		}
		return sql;
	}

	@Override
	public String buildCondition(MatHangSearch m) {
		String query = " WHERE 1 = 1";
		if(m.id !=0) {
			query += " AND id = " +m.id; 
		}
		if(!Utils.isNullOrEmpty(m.ten)) {
			query += " AND ten LIKE '%" +m.ten+"%'"; 
		}
		if(m.giaBanTu != -1) {
			query += " AND giaBan >= "+ m.giaBanTu;
		}
		if(m.giaBanDen != -1) {
			query += " AND giaBan <= "+ m.giaBanDen;
		}		
		if(m.giaNhapTu != -1) {
			query += " AND giaNhap >= "+ m.giaNhapTu;
		}
		if(m.giaNhapDen != -1) {
			query += " AND giaNhap <= "+ m.giaNhapDen;
		}
		if(m.soLuongBanTu != -1) {
			query += " AND soLuongBan >= "+ m.soLuongBanTu;
		}
		if(m.soLuongBanDen != -1) {
			query += " AND soLuongBan <= "+ m.soLuongBanDen;
		}		
		if(m.soLuongNhapTu != -1) {
			query += " AND soLuongNhap >= "+ m.soLuongNhapTu;
		}
		if(m.soLuongNhapDen != -1) {
			query += " AND soLuongNhap <= "+ m.soLuongNhapDen;
		}
		return query;
	}
	
}
