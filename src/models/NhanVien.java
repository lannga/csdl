package models;

import java.sql.ResultSet;
import java.sql.SQLException;

import models.search.NhanVienSearch;
import utils.Utils;

public class NhanVien extends BaseModel<NhanVien, NhanVienSearch> {
	
	public int idNv;
	public String tenNv;
	public String diaChiNv;
	public String statusNv ;
	public String sdtNv;
	

	public NhanVien(int idNv, String tenNv,String diaChiNv,String statusNv,String sdtNv) {
		this.idNv = idNv;
		this.tenNv=tenNv;
		this.diaChiNv=diaChiNv;
		this.statusNv=statusNv;
		this.sdtNv=sdtNv;
		
	}
	public NhanVien() {
		
	}
	
	
	
	public Object[] toArray() {
		Object arr[] = {idNv,tenNv,diaChiNv,statusNv,sdtNv ,""};
		return arr;
	}



	@Override
	public NhanVien buildObject(ResultSet rs) throws SQLException {
		// TODO Auto-generated method stub
		
		NhanVien nv = new NhanVien(
				rs.getInt(1),
				rs.getString(2),
				rs.getString(3),
				rs.getString(4),
				rs.getString(5)
				
				);		
		return nv;
	}



	@Override
	public String buildCondition(NhanVienSearch m) {
		// TODO Auto-generated method stub
		String query = " WHERE 1 = 1";
		if(m.idNv !=0) {
			query += " AND ID_NV = " +m.idNv; 
		}
		if(!Utils.isNullOrEmpty(m.diaChiNv)) {
			query += " AND ADDRESS LIKE N'%" +m.diaChiNv+"%'"; 
		}
		if(!Utils.isNullOrEmpty(m.statusNv)) {
			query += " AND STATUS LIKE N'%" +m.statusNv+"%'"; 
		}
		if(!Utils.isNullOrEmpty(m.tenNv)) {
			query += " AND NAME LIKE N'%" +m.tenNv+"%'"; 
		}
		if(!Utils.isNullOrEmpty(m.sdtNv)) {
			query += " AND PHONE LIKE N'%" +m.sdtNv+"%'"; 
		}
		
		return query;
	}



	@Override
	public String buildQuery(NhanVienSearch m) {
		String sql = "SELECT * FROM EMPLOYEES ";
		sql += buildCondition(m);
		return sql;
	}



	@Override
	public String buildUpdate(NhanVien t, NhanVienSearch s) {
		String sql = "UPDATE EMPLOYEES SET";
		if(t.tenNv != null) {
			sql += " NAME =" + "N'"+ t.tenNv+"'"+",";
		}	
		
		if(t.diaChiNv != null) {
			sql += " ADDRESS =" + "N'"+ t.diaChiNv+"'"+",";
		}	
		
		if(t.statusNv != null) {
			sql += " STATUS =" + "N'"+ t.statusNv+"'"+",";
		}	
		
		if(t.sdtNv != null) {
			sql += " PHONE =" + "N'"+ t.sdtNv+"'";
		}	
		
		if(s == null) {
			sql += " WHERE ID_NV = "+ t.idNv;
		}else {
			sql += buildCondition(s);
		}
		System.out.println(sql);
		return sql;
	}



	@Override
	public String buildInsert(NhanVien t) {
		String sql = "INSERT INTO EMPLOYEES (ID_NV,NAME,ADDRESS,STATUS,PHONE)" +
                "VALUES ("
				+t.idNv
				+" ,"+" N'"+t.tenNv +"'"
				+" ,"+" N'"+t.diaChiNv +"'"
				+" ,"+" N'"+t.statusNv+"'"
				+" ,"+" N'"+t.sdtNv+"'"
				+")";
		System.out.println(sql);
		return sql;
	}



	@Override
	public String buildDelete(NhanVien t, NhanVienSearch search) {
		String sql = "DELETE FROM EMPLOYEES";
		if(search == null) {
			sql += " WHERE ID_NV = "+ t.idNv;
		}else {
			sql += buildCondition(search);
		}
		return sql;
	}
	
		
	

	
}

