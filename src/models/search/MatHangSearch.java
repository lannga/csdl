package models.search;

public class MatHangSearch {
	public int id;
	public String ten = "";
	public double giaBanTu = -1, giaBanDen = -1;
	public double giaNhapTu = -1, giaNhapDen = -1;
	public int soLuongBanTu = -1, soLuongBanDen = -1;
	public int soLuongNhapTu = -1, soLuongNhapDen = -1;
}
