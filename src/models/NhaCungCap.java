package models;

import java.sql.ResultSet;
import java.sql.SQLException;

import models.search.NhaCungCapSearch;
import utils.Utils;

public class NhaCungCap extends BaseModel<NhaCungCap, NhaCungCapSearch> {
	
	public int idNCC;
	public String tenNCC;
	public String diaChiNCC;
	public String sdtNCC;
	

	public NhaCungCap(int idNCC, String tenNCC,String diaChiNCC,String sdtNCC) {
		this.idNCC = idNCC;
		this.tenNCC=tenNCC;
		this.diaChiNCC=diaChiNCC;
		
		this.sdtNCC=sdtNCC;
		
	}
	public NhaCungCap() {
		
	}
	
	
	
	public Object[] toArray() {
		Object arr[] = {idNCC,tenNCC,diaChiNCC,sdtNCC ,""};
		return arr;
	}
	@Override
	public NhaCungCap buildObject(ResultSet rs) throws SQLException {
		NhaCungCap ncc = new NhaCungCap(
				rs.getInt(1),
				rs.getString(2),
				rs.getString(3),
				rs.getString(4)
				);		
		return ncc;
	}
	@Override
	public String buildCondition(NhaCungCapSearch m) {
		// TODO Auto-generated method stub
				String query = " WHERE 1 = 1";
				if(m.idNCC !=0) {
					query += " AND ID_NCC = " +m.idNCC; 
				}
				if(!Utils.isNullOrEmpty(m.diaChiNCC)) {
					query += " AND ADDRESS LIKE N'%" +m.diaChiNCC+"%'"; 
				}
				
				if(!Utils.isNullOrEmpty(m.tenNCC)) {
					query += " AND NAME LIKE N'%" +m.tenNCC+"%'"; 
				}
				if(!Utils.isNullOrEmpty(m.sdtNCC)) {
					query += " AND PHONE LIKE N'%" +m.sdtNCC+"%'"; 
				}
				
				return query;
	}
	@Override
	public String buildQuery(NhaCungCapSearch m) {
		String sql = "SELECT * FROM SUPPLIER ";
		sql += buildCondition(m);
		return sql;
	}
	@Override
	public String buildUpdate(NhaCungCap t, NhaCungCapSearch s) {
		String sql = "UPDATE SUPPLIER SET";
		if(t.tenNCC != null) {
			sql += " NAME =" + "N'"+ t.tenNCC+"'"+",";
		}	
		
		if(t.diaChiNCC != null) {
			sql += " ADDRESS =" + "N'"+ t.diaChiNCC+"'"+",";
		}	
		
		
		if(t.sdtNCC != null) {
			sql += " PHONE =" + "'"+ t.sdtNCC+"'";
		}	
		
		if(s == null) {
			sql += " WHERE ID_NCC = "+ t.idNCC;
		}else {
			sql += buildCondition(s);
		}
		System.out.println(sql);
		return sql;
	}
	@Override
	public String buildInsert(NhaCungCap t) {
		String sql = "INSERT INTO SUPPLIER (ID_NCC,NAME,ADDRESS,PHONE)" +
                "VALUES ("
				+t.idNCC
				+" ,"+" N'"+t.tenNCC +"'"
				+" ,"+" N'"+t.diaChiNCC +"'"
				+" ,"+" '"+t.sdtNCC+"'"
				+")";
		System.out.println(sql);
		return sql;
	}
	@Override
	public String buildDelete(NhaCungCap t, NhaCungCapSearch search) {
		String sql = "DELETE FROM SUPPLIER";
		if(search == null) {
			sql += " WHERE ID_NCC = "+ t.idNCC;
		}else {
			sql += buildCondition(search);
		}
		return sql;
	}
	

	
		
	

	
}


